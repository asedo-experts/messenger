//
//  MessangerKit.h
//  MessangerKit
//
//  Created by Alexandr Zaliva on 19.12.2019.
//  Copyright © 2019 Asedo. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for MessangerKit.
FOUNDATION_EXPORT double MessangerKitVersionNumber;

//! Project version string for MessangerKit.
FOUNDATION_EXPORT const unsigned char MessangerKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <MessangerKit/PublicHeader.h>



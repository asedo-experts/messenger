import Foundation
import PromiseKit
import Firebase

public protocol StoreAuthLogin {
    func login(email: String, pass: String) -> Promise<AuthDataResult?>
}

class StoreAuthLoginImpl: StoreAuthLogin {
    
    init() {
        configure()
    }
    
    private func configure() {
        FirebaseApp.configure()
    }
    
    func login(email: String, pass: String) -> Promise<AuthDataResult?> {
        return Promise<AuthDataResult?> { resolver in
            Auth.auth().signIn(withEmail: email, password: pass, completion: { (result, error) in
                if let error = error {
                    resolver.reject(error)
                }
                resolver.fulfill(result)
            })
        }
    }
    
}

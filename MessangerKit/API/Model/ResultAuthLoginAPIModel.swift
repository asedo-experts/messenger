import Foundation

public struct ResultAuthLoginAPIModel: Codable, Equatable {
    let token: String
    let refreshToken: String
    let expires: String
}

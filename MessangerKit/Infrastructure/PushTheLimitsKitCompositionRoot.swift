import Foundation

public class PushTheLimitsKitCompositionRoot {
    
    public static var sharedInstance: PushTheLimitsKitCompositionRoot = PushTheLimitsKitCompositionRoot()
    
    private init() {}
    
    public lazy var resolveStoreAuthLogin: StoreAuthLogin = {
        return StoreAuthLoginImpl.init()
    }()
    
}

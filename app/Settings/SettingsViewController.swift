import UIKit

class SettingsViewController: BaseViewController<SettingsViewModel> {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func editAction(_ sender: Any) {
        self.navigationController?.pushViewController(CompositionRoot.sharedInstance.resolveEditProfileViewController(), animated: true)
    }
}


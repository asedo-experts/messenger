import UIKit
import MessangerKit

class CompositionRoot {
    
    var pushTheLimitsKitCompositionRoot: PushTheLimitsKitCompositionRoot {
        return PushTheLimitsKitCompositionRoot.sharedInstance
    }
    
    static var sharedInstance: CompositionRoot = CompositionRoot()
    
    var rootTabBarController: UITabBarController!
    
    
    required init() {
        configureRootTabBarController()
    }
    
    private func configureRootTabBarController() {
        rootTabBarController = UITabBarController()
        rootTabBarController.tabBar.isTranslucent = false
        
        var viewControllersList: [UIViewController] = [UIViewController]()
        
        let contactsViewController = resolveContactsViewController()
        contactsViewController.tabBarItem = UITabBarItem(title: "Contacts", image: UIImage(systemName: "person.crop.circle.fill"), tag: 1)
        contactsViewController.tabBarItem.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
        viewControllersList.append(contactsViewController)
        
        let callsViewController = resolveCallsViewController()
        callsViewController.tabBarItem = UITabBarItem(title: "Calls", image: UIImage(systemName: "phone.fill"), tag: 1)
        callsViewController.tabBarItem.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
        viewControllersList.append(callsViewController)
        
        let chatsViewController = resolveChatsViewController()
        chatsViewController.tabBarItem = UITabBarItem(title: "Chats", image: UIImage(systemName: "message.fill"), tag: 2)
        chatsViewController.tabBarItem.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
        viewControllersList.append(chatsViewController)
        
        let settingsViewController = resolveSettingsViewController()
        settingsViewController.tabBarItem = UITabBarItem(title: "Settings", image: UIImage(named: "settings_icon"), tag: 3)
        settingsViewController.tabBarItem.imageInsets = UIEdgeInsets(top: 3, left: 0, bottom: 0, right: 0)
        viewControllersList.append(settingsViewController)
        
        let viewControllers = viewControllersList.map { (viewController) -> UIViewController in
            let navigationController = UINavigationController(rootViewController: viewController)
            return navigationController
        }
        
        rootTabBarController.setViewControllers(viewControllers, animated: true)
    }
    
    // MARK: ViewControllers
    
    func resolveContactsViewController() -> ContactsViewController {
        let vc = ContactsViewController.instantiateFromStoryboard("Contacts")
        vc.viewModel = resolveContactsViewModel()
        return vc
    }
    
    func resolveCallsViewController() -> CallsViewController {
        let vc = CallsViewController.instantiateFromStoryboard("Calls")
        vc.viewModel = resolveCallsViewModel()
        return vc
    }
    
    func resolveChatsViewController() -> ChatsViewController {
        let vc = ChatsViewController.instantiateFromStoryboard("Chats")
        vc.viewModel = resolveChatsViewModel()
        return vc
    }
    
    func resolveSettingsViewController() -> SettingsViewController {
        let vc = SettingsViewController.instantiateFromStoryboard("Settings")
        vc.viewModel = resolveSettingsViewModel()
        return vc
    }
    
    func resolveEditProfileViewController() -> EditProfileViewController {
        let vc = EditProfileViewController.instantiateFromStoryboard("Profile")
        vc.viewModel = resolveProfileViewModel()
        return vc
    }
    
    // MARK: ViewModels
    
    func resolveContactsViewModel() -> ContactsViewModel {
        return ContactsViewModel()
    }
    
    func resolveCallsViewModel() -> CallsViewModel {
        return CallsViewModel()
    }
    
    func resolveChatsViewModel() -> ChatsViewModel {
        return ChatsViewModel()
    }
    
    func resolveProfileViewModel() -> ProfileViewModel {
        return ProfileViewModel()
    }
    
    func resolveSettingsViewModel() -> SettingsViewModel {
        return SettingsViewModel()
    }
}

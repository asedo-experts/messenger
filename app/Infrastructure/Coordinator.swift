import UIKit

class Coordinator {
    
    static let shared = Coordinator()
    
    private var compositionRoot: CompositionRoot {
        return CompositionRoot.sharedInstance
    }

    private var baseNavigationController: UINavigationController? {
        return lastPresentedViewController?.navigationController
    }
    
    private var appDelegate: AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    private var lastPresentedViewController: UIViewController?
    
    private init() {
        
    }
    
    func showRootTabBarController() {
        let tabBarController = compositionRoot.rootTabBarController
        appDelegate.window?.rootViewController = tabBarController
    }
//
    func goToEditProfileViewController() {
        push(compositionRoot.resolveEditProfileViewController())
    }
//
//    func goLoginViewController() {
//        push(compositionRoot.resolveLoginViewController())
//    }

    
//    func showRegisterController() {
//        let addPhoneViewController = compositionRoot.resolveAddPhoneViewController()
//        appDelegate.window?.rootViewController = UINavigationController(rootViewController: addPhoneViewController)
//    }
//
//    func goToCreateUsernameViewContoller() {
//        push(compositionRoot.resolveCreateUsernameViewController())
//    }
//
//
//    func goToAddPhoneViewContoller() {
//        push(compositionRoot.resolveAddPhoneViewController())
//    }
//
//    func goToCountrySelectViewController(getCountry: CountrySelectDelegate) {
//        push(compositionRoot.resolveCountrySelectViewController(getCountryDelegate: getCountry))
//    }
//
//    func goToConfirmationCodeViewContoller() {
//        push(compositionRoot.resolveConfirmationCodeViewController())
//    }
//
//    func goToFirstInfoViewContoller() {
//        push(compositionRoot.resolveFirstInfoViewController())
//    }

    func push(_ vc: UIViewController) {
        if let baseNavigationController = baseNavigationController {
            baseNavigationController.pushViewController(vc, animated: true)
            lastPresentedViewController = vc
        } else {
            if let navigationController = appDelegate.window?.rootViewController as? UINavigationController {
                navigationController.pushViewController(vc, animated: true)
            }
        }
    }
    
}

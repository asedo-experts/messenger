import UIKit

class BaseViewController<T>: UIViewController, BaseMessangerUI where T: ViewModel {
    var observer = Observer()

    var viewModel: T!

    public override func viewDidLoad() {
        super.viewDidLoad()

        // If we are inside a navigation bar, we hide it.
//        navigationController?.navigationBar.isHidden = true
//        navigationController?.interactivePopGestureRecognizer?.isEnabled = false

        // didEnterBackgroundNotification
        NotificationCenter.default.addObserver(self, selector: #selector(willEnterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(didEnterBackground), name: UIApplication.didEnterBackgroundNotification, object: nil)

        viewModel.viewDidLoad()
    }

    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        bindWithObserver()
        configureColors()
        viewModel.viewWillAppear()
    }

    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.viewDidAppear()
    }

    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        viewModel.viewWillDisappear()
        observer.invalidateAll()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        viewModel.viewDidDisappear()
    }

    @objc func willEnterForeground() {
        viewModel.willEnterForeground()
    }

    @objc func didEnterBackground() {
        viewModel.didEnterBackground()
    }

    func bindWithObserver() {}

    func set(viewModel: T) {
        self.viewModel = viewModel
    }
    
    func configureColors() {
//        self.tabBarController?.tabBar.barTintColor = Theme.current.mainColor
//        self.tabBarController?.tabBar.tintColor = Theme.current.additionalColor
//        self.navigationController?.navigationBar.barTintColor = Theme.current.mainColor
//        self.navigationItem.titleView?.tintColor = Theme.current.additionalColor
    }
}

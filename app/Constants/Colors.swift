// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
  import AppKit
  internal enum ColorName { }
#elseif os(iOS) || os(tvOS) || os(watchOS)
  import UIKit
  internal enum ColorName { }
#endif

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// MARK: - Colors

// swiftlint:disable identifier_name line_length type_body_length
internal extension ColorName {
  /// 0xde3434ff (r: 222, g: 52, b: 52, a: 255)
  internal static let red = #colorLiteral(red: 0.87058824, green: 0.20392157, blue: 0.20392157, alpha: 1.0)
}
// swiftlint:enable identifier_name line_length type_body_length

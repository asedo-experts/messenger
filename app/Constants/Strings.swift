// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// MARK: - Strings

// swiftlint:disable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:disable nesting type_body_length type_name
internal enum L10n {

  internal enum App {
    /// Userlytics App
    internal static let name = L10n.tr("Localizable", "app.name")
    internal enum Error {
      internal enum Server {
        internal enum Down {
          /// The server seems to be unavailable now. Try again later.
          internal static let message = L10n.tr("Localizable", "app.error.server.down.message")
        }
      }
      internal enum Unknown {
        /// There are some issues, why don't you try later? :)
        internal static let message = L10n.tr("Localizable", "app.error.unknown.message")
      }
    }
    internal enum Notification {
      internal enum Error {
        /// Error
        internal static let title = L10n.tr("Localizable", "app.notification.error.title")
      }
      internal enum Info {
        /// Info
        internal static let title = L10n.tr("Localizable", "app.notification.info.title")
      }
    }
  }

  internal enum Checkhardware {
    internal enum Screen {
      internal enum Audiobar {
        internal enum Enough {
          /// You've reached the amount of voice required to continue
          internal static let level = L10n.tr("Localizable", "checkhardware.screen.audiobar.enough.level")
        }
        internal enum Not {
          internal enum Enough {
            /// Speak louder to be able to continue
            internal static let level = L10n.tr("Localizable", "checkhardware.screen.audiobar.not.enough.level")
          }
        }
      }
      internal enum Button {
        /// FINISH PREVIEW
        internal static let finishpreview = L10n.tr("Localizable", "checkhardware.screen.button.finishpreview")
      }
      internal enum Label {
        internal enum Audio {
          /// Your Audio Test
          internal static let title = L10n.tr("Localizable", "checkhardware.screen.label.audio.title")
        }
        internal enum Saysomething {
          /// Say Something
          internal static let title = L10n.tr("Localizable", "checkhardware.screen.label.saysomething.title")
        }
        internal enum Webcam {
          /// Your Webcam Test
          internal static let title = L10n.tr("Localizable", "checkhardware.screen.label.webcam.title")
        }
      }
      internal enum Notification {
        internal enum Text {
          /// This test requires your microphone to be enabled. Go to Settings and please enable it.
          internal static let failingmicrophone = L10n.tr("Localizable", "checkhardware.screen.notification.text.failingmicrophone")
          /// This test requires your front camera to be enabled. Please enable it.
          internal static let failingwebcam = L10n.tr("Localizable", "checkhardware.screen.notification.text.failingwebcam")
          /// There is a problem using your webcam.
          internal static let failingwebcamgeneric = L10n.tr("Localizable", "checkhardware.screen.notification.text.failingwebcamgeneric")
        }
      }
    }
  }

  internal enum Information {
    internal enum Screen {
      internal enum Button {
        /// START
        internal static let start = L10n.tr("Localizable", "information.screen.button.start")
      }
    }
  }

  internal enum Login {
    internal enum Screen {
      internal enum Button {
        /// LOGIN
        internal static let `continue` = L10n.tr("Localizable", "login.screen.button.continue")
        /// Forgot Password
        internal static let forgotpassword = L10n.tr("Localizable", "login.screen.button.forgotpassword")
        /// I have a code
        internal static let ihavecode = L10n.tr("Localizable", "login.screen.button.ihavecode")
        /// LOGIN
        internal static let login = L10n.tr("Localizable", "login.screen.button.login")
        /// SIGN UP
        internal static let signup = L10n.tr("Localizable", "login.screen.button.signup")
      }
      internal enum Label {
        /// Don't have an account?
        internal static let donthaveaccount = L10n.tr("Localizable", "login.screen.label.donthaveaccount")
        /// Welcome! Your opinion matters.
        internal static let welcome = L10n.tr("Localizable", "login.screen.label.welcome")
      }
      internal enum Textfield {
        internal enum Placeholder {
          /// Email
          internal static let email = L10n.tr("Localizable", "login.screen.textfield.placeholder.email")
          /// Password
          internal static let password = L10n.tr("Localizable", "login.screen.textfield.placeholder.password")
        }
      }
    }
  }

  internal enum Main {
    internal enum Screen {
      internal enum Button {
        /// Back to Login Screen
        internal static let backtologin = L10n.tr("Localizable", "main.screen.button.backtologin")
        /// Back to Test List Screen
        internal static let backtotestlist = L10n.tr("Localizable", "main.screen.button.backtotestlist")
        /// CONTINUE
        internal static let `continue` = L10n.tr("Localizable", "main.screen.button.continue")
        /// Need Code?
        internal static let needcode = L10n.tr("Localizable", "main.screen.button.needcode")
        /// Terms of Service
        internal static let termsofservice = L10n.tr("Localizable", "main.screen.button.termsofservice")
      }
      internal enum Checkbox {
        internal enum Accesibility {
          internal enum Accept {
            /// Tap to accept terms of use
            internal static let terms = L10n.tr("Localizable", "main.screen.checkbox.accesibility.accept.terms")
          }
        }
      }
      internal enum Label {
        /// Please insert the code:
        internal static let pleaseinsertcode = L10n.tr("Localizable", "main.screen.label.pleaseinsertcode")
        /// Welcome! Your opinion matters.
        internal static let welcome = L10n.tr("Localizable", "main.screen.label.welcome")
        internal enum Accept {
          /// I accept the 
          internal static let terms = L10n.tr("Localizable", "main.screen.label.accept.terms")
        }
        internal enum Error {
          /// The test you are trying to take must be conducted on a dektop. Please go to your dashboard to see test details
          internal static let desktop = L10n.tr("Localizable", "main.screen.label.error.desktop")
          internal enum Android {
            /// The test you are trying to take must be conducted on a android mobile. Please go to your dashboard to see test details
            internal static let mobile = L10n.tr("Localizable", "main.screen.label.error.android.mobile")
            /// The test you are trying to take must be conducted on a android tablet. Please go to your dashboard to see test details
            internal static let tablet = L10n.tr("Localizable", "main.screen.label.error.android.tablet")
          }
          internal enum Ios {
            /// The test you are trying to take must be conducted on a iOS mobile. Please go to your dashboard to see test details
            internal static let mobile = L10n.tr("Localizable", "main.screen.label.error.ios.mobile")
            /// The test you are trying to take must be conducted on a iOS tablet. Please go to your dashboard to see test details
            internal static let tablet = L10n.tr("Localizable", "main.screen.label.error.ios.tablet")
          }
        }
      }
      internal enum Textfield {
        internal enum Placeholder {
          /// Unique Code
          internal static let insertcode = L10n.tr("Localizable", "main.screen.textfield.placeholder.insertcode")
        }
      }
    }
  }

  internal enum Native {
    internal enum Error {
      internal enum Replaykit {
        /// ReplayKit seems to be unable to fetch your screen video. Please stop it and start it again sending your app to background before.
        internal static let failed = L10n.tr("Localizable", "native.error.replaykit.failed")
      }
    }
  }

  internal enum Passwordrecovery {
    internal enum Screen {
      internal enum Button {
        /// Back to Login Screen
        internal static let backtologin = L10n.tr("Localizable", "passwordrecovery.screen.button.backtologin")
        /// REQUEST PASSWORD
        internal static let `continue` = L10n.tr("Localizable", "passwordrecovery.screen.button.continue")
      }
      internal enum Label {
        /// Please enter your email:
        internal static let enteremail = L10n.tr("Localizable", "passwordrecovery.screen.label.enteremail")
      }
      internal enum Textfield {
        internal enum Placeholder {
          /// Email
          internal static let email = L10n.tr("Localizable", "passwordrecovery.screen.textfield.placeholder.email")
        }
      }
    }
  }

  internal enum Preview {
    internal enum Screen {
      internal enum Button {
        /// PREVIEW
        internal static let `continue` = L10n.tr("Localizable", "preview.screen.button.continue")
      }
    }
  }

  internal enum Signup {
    internal enum Screen {
      internal enum Button {
        /// Back to Login Screen
        internal static let backtologin = L10n.tr("Localizable", "signup.screen.button.backtologin")
        /// SIGN UP
        internal static let signup = L10n.tr("Localizable", "signup.screen.button.signup")
      }
      internal enum Label {
        /// I confirm I’m <bold>16 years</bold> of age or older
        internal static let confirm16years = L10n.tr("Localizable", "signup.screen.label.confirm16years")
        /// I accept the 
        internal static let iAcceptThe = L10n.tr("Localizable", "signup.screen.label.iAcceptThe")
        /// Terms of Service & Privacy Policy
        internal static let termsOfServiceThePrivacyPolicy = L10n.tr("Localizable", "signup.screen.label.termsOfServiceThePrivacyPolicy")
        /// Get <pink>PAID</pink> to test websites & apps!
        internal static let welcome = L10n.tr("Localizable", "signup.screen.label.welcome")
        /// <pink>*</pink> You will receive an email to create your <pink>password</pink>
        internal static let willreceiveanemail = L10n.tr("Localizable", "signup.screen.label.willreceiveanemail")
      }
      internal enum Textfield {
        internal enum Placeholder {
          /// Email
          internal static let email = L10n.tr("Localizable", "signup.screen.textfield.placeholder.email")
        }
      }
    }
  }

  internal enum Terms {
    internal enum Of {
      internal enum Service {
        /// Loading terms of service information
        internal static let activityindicator = L10n.tr("Localizable", "terms.of.service.activityindicator")
        /// Terms of service
        internal static let title = L10n.tr("Localizable", "terms.of.service.title")
        internal enum Button {
          /// ACCEPT
          internal static let accept = L10n.tr("Localizable", "terms.of.service.button.accept")
        }
      }
    }
  }

  internal enum Test {
    internal enum Screen {
      internal enum Notification {
        /// That address seems to be invalid.
        internal static let invalidurl = L10n.tr("Localizable", "test.screen.notification.invalidurl")
        /// You are about to leave the Userlytics app. Remember to come back to the app to continue reading the instructions.
        internal static let leavingapp = L10n.tr("Localizable", "test.screen.notification.leavingapp")
        /// The allotted time for this test has been reached, and we will now upload the results. Do not worry if you could not finish and thanks for helping make the world more user friendly!
        internal static let timeout = L10n.tr("Localizable", "test.screen.notification.timeout")
        internal enum Button {
          /// OPEN
          internal static let `open` = L10n.tr("Localizable", "test.screen.notification.button.open")
          /// RETRY
          internal static let retry = L10n.tr("Localizable", "test.screen.notification.button.retry")
        }
        internal enum Error {
          /// There is a problem with the test URL.
          internal static let invalidurl = L10n.tr("Localizable", "test.screen.notification.error.invalidurl")
          /// Audio recording is required for this test. Please start again and grant that permission when requested. Should the issue happen again, please reboot your device.
          internal static let noAudioAvailable = L10n.tr("Localizable", "test.screen.notification.error.noAudioAvailable")
          /// There was an error fetching the screen content. Please reboot your device.
          internal static let noVideoAvailable = L10n.tr("Localizable", "test.screen.notification.error.noVideoAvailable")
          /// Update your device to at least iOS 11 to run the test.
          internal static let olddevice = L10n.tr("Localizable", "test.screen.notification.error.olddevice")
          /// Unable to start the screen recording. Try to reboot your device.
          internal static let unabletostartscreenrecording = L10n.tr("Localizable", "test.screen.notification.error.unabletostartscreenrecording")
          /// You must accept the permissions to be able to run the test.
          internal static let userrejects = L10n.tr("Localizable", "test.screen.notification.error.userrejects")
        }
        internal enum Leavingapp {
          /// Ok
          internal static let ok = L10n.tr("Localizable", "test.screen.notification.leavingapp.ok")
          /// Leaving Userlytics
          internal static let title = L10n.tr("Localizable", "test.screen.notification.leavingapp.title")
        }
        internal enum Timeout {
          /// Time's up!
          internal static let title = L10n.tr("Localizable", "test.screen.notification.timeout.title")
        }
      }
    }
    internal enum Window {
      /// Userlytics Tasks
      internal static let title = L10n.tr("Localizable", "test.window.title")
    }
  }

  internal enum Testlist {
    internal enum Screen {
      internal enum Label {
        /// Approved
        internal static let approved = L10n.tr("Localizable", "testlist.screen.label.approved")
        /// You don’t have any approved tests yet
        internal static let approvedEmptyWarning = L10n.tr("Localizable", "testlist.screen.label.approvedEmptyWarning")
        /// Completed Tests
        internal static let completedtests = L10n.tr("Localizable", "testlist.screen.label.completedtests")
        /// Missed Invites
        internal static let missedinvites = L10n.tr("Localizable", "testlist.screen.label.missedinvites")
        /// You do not have any pending tests
        internal static let pendingEmptyWarning = L10n.tr("Localizable", "testlist.screen.label.pendingEmptyWarning")
        /// Pending Review
        internal static let pendingreview = L10n.tr("Localizable", "testlist.screen.label.pendingreview")
        /// Rejected
        internal static let rejected = L10n.tr("Localizable", "testlist.screen.label.rejected")
        /// You don’t have any rejected tests
        internal static let rejectedEmptyWarning = L10n.tr("Localizable", "testlist.screen.label.rejectedEmptyWarning")
        /// Currently there are not active test invitations that correspond to your demographic profile; we will notify you via email as soon as the next one is available
        internal static let testInvitationsEmptyWarning = L10n.tr("Localizable", "testlist.screen.label.testInvitationsEmptyWarning")
        /// Test Invitations
        internal static let testsinvitations = L10n.tr("Localizable", "testlist.screen.label.testsinvitations")
      }
      internal enum Warning {
        internal enum Cannotlaunchtest {
          /// This test has to be launched on an Android device, please log on to your computer to launch it
          internal static let android = L10n.tr("Localizable", "testlist.screen.warning.cannotlaunchtest.android")
          /// This test has to be launched on a computer, please log on to your computer to launch it
          internal static let desktop = L10n.tr("Localizable", "testlist.screen.warning.cannotlaunchtest.desktop")
          /// This test has to be launched on an iOS device, please log on to your computer to launch it
          internal static let ios = L10n.tr("Localizable", "testlist.screen.warning.cannotlaunchtest.ios")
        }
      }
    }
  }

  internal enum Upload {
    internal enum Screen {
      internal enum Label {
        /// Encoding video...
        internal static let encodingvideo = L10n.tr("Localizable", "upload.screen.label.encodingvideo")
        /// Ready
        internal static let ready = L10n.tr("Localizable", "upload.screen.label.ready")
        /// Uploading...
        internal static let uploading = L10n.tr("Localizable", "upload.screen.label.uploading")
      }
    }
  }

  internal enum Uploaded {
    internal enum Screen {
      internal enum Button {
        /// FINISH
        internal static let close = L10n.tr("Localizable", "uploaded.screen.button.close")
        /// support@userlytics.com
        internal static let email = L10n.tr("Localizable", "uploaded.screen.button.email")
        /// RETRY
        internal static let retry = L10n.tr("Localizable", "uploaded.screen.button.retry")
        internal enum Error {
          /// RESTART TEST
          internal static let restart = L10n.tr("Localizable", "uploaded.screen.button.error.restart")
        }
      }
      internal enum Notification {
        internal enum Recording {
          /// The recording was cancelled. Restart it.
          internal static let aborted = L10n.tr("Localizable", "uploaded.screen.notification.recording.aborted")
        }
      }
      internal enum Text {
        /// Thank you!
        internal static let thankyou = L10n.tr("Localizable", "uploaded.screen.text.thankyou")
        /// UPLOADED!
        internal static let uploaded = L10n.tr("Localizable", "uploaded.screen.text.uploaded")
        internal enum Error {
          /// Recording was cancelled!
          internal static let aborted = L10n.tr("Localizable", "uploaded.screen.text.error.aborted")
          /// Test already uploaded!
          internal static let alreadyUploaded = L10n.tr("Localizable", "uploaded.screen.text.error.alreadyUploaded")
          /// Error processing video!
          internal static let errorprocessingvideo = L10n.tr("Localizable", "uploaded.screen.text.error.errorprocessingvideo")
          /// Invalid video found.
          internal static let invalidvideo = L10n.tr("Localizable", "uploaded.screen.text.error.invalidvideo")
          /// Not uploaded!
          internal static let notuploaded = L10n.tr("Localizable", "uploaded.screen.text.error.notuploaded")
          /// ERROR!
          internal static let title = L10n.tr("Localizable", "uploaded.screen.text.error.title")
          /// Try again later
          internal static let unknown = L10n.tr("Localizable", "uploaded.screen.text.error.unknown")
        }
        internal enum Ifyouhaveanyissues {
          /// If you have any issues or questions\nyou can reach us at
          internal static let all = L10n.tr("Localizable", "uploaded.screen.text.ifyouhaveanyissues.all")
          /// any issues or questions
          internal static let highlighted = L10n.tr("Localizable", "uploaded.screen.text.ifyouhaveanyissues.highlighted")
        }
      }
    }
  }
}
// swiftlint:enable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:enable nesting type_body_length type_name

// MARK: - Implementation Details

extension L10n {
  fileprivate static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    // swiftlint:disable:next nslocalizedstring_key
    let format = NSLocalizedString(key, tableName: table, bundle: Bundle(for: BundleToken.self), comment: "")
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

private final class BundleToken {}
